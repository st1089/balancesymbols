/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balancesymbols;

import java.util.Stack;
import java.util.*;

public class Balancesymbols {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Type  your expression");
        String check = in.next();
        
         System.out.println(compare(check));
//        (5+4)-[5+(3+2)]

    }

    public static boolean compare(String check) {
        Stack<Character> stack = new Stack<Character>();for (int i = 0; i < check.length(); i++) {
            char ch = check.charAt(i);
            if (ch == '[' || ch == '{' || ch == '(') {
                stack.push(ch);
            } else if (ch == ']' || ch == '}' || ch == ')') {
                
                if (stack.empty()) {
                    return false;

                }
                switch (ch) {

                    case ']':
                        if (stack.pop() != '[') {
                            return false;
                        }
                        break;

                    case '}':
                        if (stack.pop() != '{') {
                            return false;
                        }
                        break;

                    case ')':
                        if (stack.pop() != '(') {
                            return false;
                        }
                        break;
                    default:
                        break;

                }

            }
        }

        if (stack.empty()) {
            return true;

        }
        System.out.println("Symbols are not balanced");
        return false;
       

    }

}
